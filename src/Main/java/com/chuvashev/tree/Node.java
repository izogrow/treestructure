package com.chuvashev.tree;

class Node<T> {
    private int index; // ключ узла
    private T value;
    private Node<T> leftChild; // Левый узел потомок
    private Node<T> rightChild; // Правый узел потомок

    public Node() {
    }

    public Node(T value) {
        this.value = value;
    }

    public void printNode() { // Вывод значения узла в консоль
        System.out.println(" Выбранный узел имеет значение :" + value);
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(final int index) {
        this.index = index;
    }

    public Node<T> getLeftChild() {
        return this.leftChild;
    }

    public void setLeftChild(final Node<T> leftChild) {
        this.leftChild = leftChild;
    }

    public Node<T> getRightChild() {
        return this.rightChild;
    }

    public void setRightChild(final Node<T> rightChild) {
        this.rightChild = rightChild;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "index=" + index +
                ", leftChild=" + leftChild +
                ", rightChild=" + rightChild +
                '}';
    }
}
