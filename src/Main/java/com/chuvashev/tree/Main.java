package com.chuvashev.tree;

public class Main {
    public static void main(String[] args) {
        Tree<String> tree = new Tree<>();
        tree.insertNode(4, "Hello from tree! 4");
        tree.insertNode(1, "Hello from tree!");
        tree.insertNode(2, "Hello from tree!");
        tree.insertNode(3, "Hello from tree!");
        tree.insertNode(7, "Hello from tree!");
        tree.insertNode(5, "Hello from tree!");
        tree.insertNode(6, "Hello from tree! 6");
        tree.printTree();
        System.out.println(tree.getValue(6));
    }
}
